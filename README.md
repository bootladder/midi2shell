# midi2shell : Control your computer with a MIDI Controller ! #

call amidi ... and pipe it into the python script.  The python script parses the 3 MIDI bytes to get a single number which is used as an index to a table of shell scripts.  That shell script is executed.

I like to use the xdotool , which allows me to paste text snippets into my terminal.

Use it with Synergy !